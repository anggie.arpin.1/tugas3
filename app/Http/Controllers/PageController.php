<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('index',[
            "header" => "My Profile"
        ]);
    }

    public function page1(){
        return view('Portofolio.portofolio1',[
            "header" => "My Product",
            "judul" => "Semesta Kita",
            "img" => "1.jpg",
            "link" => "https://semesta.id/",
            "deskripsi" => "Sistem Manajemen Sampah Terpadu yang kemudian disebut dengan SEMESTA KITA adalah sebuah sistem informasi manajemen yang membantu program bank sampah untuk dapat melakukan kegiatan administrasi pengelolaan sampah menjadi lebih efektif dan efisien. SEMESTA KITA dapat melakukan manajemen data anggota, pembukuan, data keluar-masuk, manajemen data sampah, promosi produk, serta manajemen iuran, sehingga pengelola serta anggota bank sampah dapat melakukan aktivitas manajemen dengan lebih mudah.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ]);
    }

    public function page2(){
        return view('Portofolio.portofolio2',[
            "header" => "My Product",
            "judul" => "SSO Informatics",
            "img" => "2.jpg",
            "link" => "https://if.undiksha.ac.id/",
            "deskripsi" => "SSO Informatics adalah Sistem Yang dimiliki oleh HMJ TI Undiksha.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ]);
    }

    public function page3(){
        return view('Portofolio.portofolio3',[
            "header" => "My Product",
            "judul" => "Prototype Application",
            "img" => "3.jpg",
            "link" => "https://www.figma.com/file/xkeeLL6yJm8TxqChOB8sQM/New-Rudaya?node-id=7%3A2",
            "deskripsi" => "Ini adalah Prototype Aplikasi Rudaya ~ Connect The Art.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ]);
    }
}
